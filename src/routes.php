<?php
$routes = [
    [
        "match" => "/sendEmail",
        "method" => "POST",
        "controller" => "EmailController",
        "function" => "sendEmail"
    ],
    [
        "match" => "/default",
        "method" => "GET",
        "controller" => "DefaultController",
        "function" => "default"
    ]
];