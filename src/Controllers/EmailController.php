<?php 

class EmailController {
    public function sendEmail() {
        $response = new Response();

        if(isset($_POST['name']) && isset($_POST['species']) && isset($_POST['email'])){
            $pet = new Pet(json_decode(file_get_contents('https://cat-fact.herokuapp.com/facts/random')), $_POST['name']);

            $mail_status = mail($_POST['email'], "Raport na temat zwierzęcia", $pet);

            $db = new Database();
            $db_status = $db->add_sended_email((string)$pet);

            
            $response->http_code = 200;
            $response->body = "Everything OK";
            
        } else {
            $response->http_code = 400;
            $response->body = "Lack of information. name, species and email fields are required.";
        }
        $response->answer();
    }
}