<?php

class Response {
    public int $http_code = 200;
    public string $body = "";

    public function answer(){
        http_response_code($this->http_code);
        echo $this->body;
    }
}