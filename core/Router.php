<?php

class Router {

    private $routes;

    public function match($request_uri, $request_method){
        foreach($this->routes as $route){
            // var_dump($route, $request_uri);
            if($route['match'] == $request_uri && $route['method'] == $request_method){
                return $route;
            }
        }
        return 404;
    }
    public function __construct($routes){
        $this->routes = $routes;
    }
}