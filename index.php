<?php
spl_autoload_register(function ($class_name) {
    if(str_contains($class_name, "Controller")){
        include './src/Controllers/'.$class_name.".php";
    }
});

require_once './core/Model.php';
require_once './core/Database.php';
require_once './core/Response.php';

require_once './core/Router.php';
require_once './src/routes.php';

require_once './src/Models/Pet.php';

$GLOBALS['db_address'] = "localhost";
$GLOBALS['db_user'] = "mkulak0";
$GLOBALS['db_password'] = "zaq1@WSX";
$GLOBALS['db_name'] = "recruitment_task";

$router = new Router($routes);

$result = $router->match($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);

if($result != 404){
    $controller = new $result['controller']();
    $controller->{$result['function']}();
} else if($result == 404){
    http_response_code(404);
    echo "404 URL don't exists";
}